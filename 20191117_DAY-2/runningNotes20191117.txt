Agenda:

1. AWS Concepts

2. SDLC

3. DevOps

4. Required Tools 

DevOps Tools :

    1. SCM/VCS :
        1. gitbash : https://git-scm.com/

        2. Windows/MacOS : CMD, PowerShell ; Terminal 

        3. IDE Tools : Spring Tool Suite, Visual Studio Code or ATOM etc... 

        4. Virtualization Tools : Oracle VirtualBox, Vmware etc... 

        5. Download Linux Distribution i.e. CentOS7 & Ubuntu 18.04

        6. Create Account With Github 

        7. Create Account with hub.docker.com 

